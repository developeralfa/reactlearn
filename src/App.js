import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import FirstComponent from './components/learning_examples/FirstComponent'
import SecondComponent from './components/learning_examples/SecondComponent'
import Counter from './components/counter/counter'

import MyCount from './components/counter/myClasses/myCount'
class App extends Component {
  render() {
    return(
      <div className="App">
       <MyCount />
      </div>
    )
  }
}
const LearningComponents = ()=>{
  return (
    <div className="Learning">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
      </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
      </a>
      </header>
      <FirstComponent />
      <SecondComponent />
      <ThirdComponent/>
      <FourthComponent/>
    </div>
  );
}

class ThirdComponent extends Component
{
  render()
  {
    return (
      <div class = "ThirdComponent">
        <marquee>Dance</marquee>
      </div>
    );
  }
}
function FourthComponent()
{
  return (
    <div className = "Fourth">
      four
    </div>
  );
}
export default App;