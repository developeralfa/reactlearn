import React, {Component} from 'react'
import propTypes from 'prop-types'
import MyCount from './myCount'
import './../counter.css'
var num = 0;
export default class myButton extends Component{
    constructor()
    {
        super()
        //this.increment = this.increment.bind(this);
    }
    render = () =>
    {
        return (
            <div className="counter">
                <button onClick={this.increment}>+{this.props.by}</button>
                <button onClick={this.decrement}>-{this.props.by}</button>
                {/* <span className="count">{this.state.counter}</span> */}
            </div>
        )
    }
    increment = () =>
    {
        //console.log("Counter");
        //this.props.counter.changeValue(this.props.by)
        this.props.changeValueMethod(this.props.by)
    }
    decrement = () =>
    {
        //console.log("Counter");
        //this.props.counter.changeValue(0-this.props.by)
        this.props.changeValueMethod(0-this.props.by)

    }
    
} 
myButton.defaultProps = {
    by: 1
}

myButton.propTypes = {
    by: propTypes.number
}
