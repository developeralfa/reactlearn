import React, {Component} from 'react'
import propTypes from 'prop-types'
import MyButton from './myButton'
import './../counter.css'
export default class myCount extends Component{
    constructor()
    {
        super()
        //this.increment = this.increment.bind(this);
        this.state = {
            value: 0
        }
    }
    render = () =>
    {
        return (
            <div className="counter">
                <MyButton by={5} changeValueMethod = {this.changeValue}/>
                <MyButton by={10} changeValueMethod = {this.changeValue}/>
                <MyButton by={20} changeValueMethod = {this.changeValue}/>
                <div className="count">{this.state.value}</div>
            </div>
        )
    }
    changeValue = (num)=>{
        this.setState({
            value: this.state.value + num
        })
    }
} 
