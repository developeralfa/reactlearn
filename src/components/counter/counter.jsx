import React, {Component} from 'react'
import propTypes from 'prop-types'
import './counter.css'
var num = 0;
export default class Counter extends Component{
    constructor()
    {
        super()
        this.state = {
            counter: 0
        }
        this.increment = this.increment.bind(this);
    }
    render = () =>
    {
        return (
            <div className="counter">
                <button onClick={this.increment}>+{this.props.by}</button>
                <span className="count">{this.state.counter}</span>
            </div>
        )
    }
    increment = () =>
    {
        //console.log("Counter");
        this.setState({
            counter: this.state.counter+this.props.by
        })
    }
    
} 
Counter.defaultProps = {
    by: 1
}

Counter.propTypes = {
    by: propTypes.number
}
